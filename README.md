# Test Frontend 2

The idea with this test is to allow us to better comprehend the skills of candidates to developer jobs, in various experience levels.

This test should be completed by you on your own, in your own time. Take as much time as you need, but usually this test shouldn't take more than a couple of hours to be done.

## Delivery instructions

Download the instructions and example images from this repo and, once done, put all project files in a .zip file and send it to `apply@nuuvem.recruitee.com`.

## Project description

Implement a responsive HTML page based on the designs below. The design and usability should consider both desktop and mobile use cases (i.e. use a responsive design). Also, remember to cater to different pixel-density devices (with high DPI displays).

Here's the Adobe XD specs for the design: https://xd.adobe.com/view/73e5331e-7361-4b70-76cd-ec0cf3eb3c2d-05d4/grid/

And screenshots of the XD spec above:

Desktop design:

![desktop version](https://gitlab.com/nuuvem-public/tests/frontend-2/-/raw/main/desktop.png)

Mobile designs:

![mobile landscape](https://gitlab.com/nuuvem-public/tests/frontend-2/-/raw/main/mobile_landscape.png)

![mobile portrait](https://gitlab.com/nuuvem-public/tests/frontend-2/-/raw/main/mobile_portrait.png)

### Implementation

You should deliver this project as a static HTML page, with related images, CSS and JS files if needed. You may use any build tools that you feel are necessary (if any), do all by hand or use whatever library or framework you judge necessary. It's up to you.

You may use any (open source) framework or library that you want. You may also only use the standard language and library, without any additional third-party code. Again, it's up to you. Use what you think is best for the task.

But treat the the code (HTML and CSS) as production-level code.

Your project MUST:

1. Be well documented
1. Use semantic markup
1. Have a consistent style and nomenclature in your code
1. Be mindful of performance
1. Be simple to configure and execute, running on a Unix-compatible environment (Linux or macOS)
1. Use only free / open-source languages and libraries

## Review

Your project will be evaluated by the following criteria:

1. Does the project fulfill the basic requirements?
1. Did you document how to configure, build or run the project?
1. Did you follow closely the project specification?
1. Quality of the user experience, in terms of usability, accessibility and overall visual presentation;
1. Quality of the code itself, how it's strutured and how it complies with good, modern practices;
1. Familiarity with the standard libraries of the languages used and other packages;
